-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2015 at 03:03 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `call_status`
--

CREATE TABLE IF NOT EXISTS `call_status` (
  `id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `session_id` varchar(1028) NOT NULL,
  `token` varchar(1028) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `call_status`
--

INSERT INTO `call_status` (`id`, `from`, `to`, `session_id`, `token`, `status`) VALUES
(3, 'a', 'b', '1_MX40NTMyODA5Mn5-MTQ0MTY3OTkyODU0N35vSkQrc2Yvcyt1SDRucWRLeXRPS0x0U01-fg', 'T1==cGFydG5lcl9pZD00NTMyODA5MiZzaWc9NmM2MDYwZDQzMzNhNmUyNmQzZTc2NTE0MWU3OTNhZTMxODk2YTE1ODpzZXNzaW9uX2lkPTFfTVg0ME5UTXlPREE1TW41LU1UUTBNVFkzT1RreU9EVTBOMzV2U2tRcmMyWXZjeXQxU0RSdWNXUkxlWFJQUzB4MFUwMS1mZyZjcmVhdGVfdGltZT0xNDQxNjc5OTI4JnJvbGU9bW9kZXJhdG9yJm5vbmNlPTE0NDE2Nzk5MjguMzIyODIwNDA3NjMxNDImZXhwaXJlX3RpbWU9MTQ0MjI4NDcyOCZjb25uZWN0aW9uX2RhdGE9bmFtZSUzREpvaG5ueQ==', 'Call');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `last_time` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_id`, `last_time`) VALUES
(1, 'a', '2015-09-07 02:56:11am'),
(2, 'b', '2015-09-07 02:56:19am'),
(3, 'c', '2015-09-07 04:31:01am'),
(4, 'd', '2015-09-07 04:02:02am'),
(5, 'e', '2015-09-08 08:31:35am'),
(6, 'codeatena', '2015-09-09 03:28:57pm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `call_status`
--
ALTER TABLE `call_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `call_status`
--
ALTER TABLE `call_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
