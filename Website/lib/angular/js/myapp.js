var app = angular.module('CallApp', ['ngDialog']);
app.controller('CallCtrl', function ($scope, $http, ngDialog) {
    $scope.isLogin = false;
    $scope.from_user_id = "";
    $scope.to_user_id = "";
    $scope.loginInf = "Please Login as user1 or user2";
    $scope.isOnline = false;
    $scope.isTalking = false;
    var interval = 5000;
    var timeout = 60000;
    var callingDialog;
    var outCallDialog;
    var curStatus = {};
    var isTalking = false;
    var session;

    function connectOpentTok(sessionId, token) {

        var apiKey = "45328092";
        session = OT.initSession(apiKey, sessionId);

        session.on("streamCreated", function(event) {
            session.subscribe(event.stream, 'subscriber', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
            });

        });
        session.connect(token, function(error) {

            if (!error) {
                var publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%'
                });
                session.publish(publisher);
            } else {
                console.log('There was an error connecting to the session:', error.code, error.message);
            }

        });
    }

    function myTimer() {
        var url = "../Backend/get_buddy_status.php";
        var postData = {};
        postData.user_id = $scope.from_user_id;
        console.log("get call status params: " + JSON.stringify(postData));

        $http.post(url, postData).success(function(response) {
            var buddy_list = response.buddy_list;
            var flag = false;
            if($scope.from_user_id == "user1") {
                if(buddy_list[0].user2 == "Offline") {
                    flag = false;
                } else
                    flag = true;
            } else {
                if(buddy_list[0].user1 == "Offline")
                    flag = false;
                else
                    flag = true;
            }

            if (flag)
                $scope.isOnline = true;
            else
                $scope.isOnline = false;

            console.log("isLogin?  :" + $scope.isOnline);
            curStatus = {};
            curStatus = response;
            checkOutCallback();
        });
    }

    function checkOutCallback() {
        var out_status;
        var in_status;

        try {
            out_status = curStatus.outcoming_call[0].status;
        } catch (error) { }

        try {
            in_status = curStatus.incoming_call[0].status;
        } catch (error) { }

        console.log(out_status);
        console.log(in_status);
        switch (out_status)
        {
            case "Call":
                onCall();
                break;
            default :
        }

        switch (in_status)
        {
            case "Accept":

                onAccept();
                break
            case "Reject":
                onReject();
                break
            case "End":
                onEnd();
                break;
            default :
        }

    }

    function onCall() {
        if (isTalking == true) {
            return;
        }
        isTalking = true;
        outCallDialog = ngDialog.openConfirm({
            template: 'outComingCallDialogId',
            scope: $scope
        });
    }

    function onAccept() {

        ngDialog.close(callingDialog);
        $scope.isTalking = true;
    }

    function onReject() {
        ngDialog.close(callingDialog);
        $scope.isTalking = false;
    }

    function onEnd() {

    }
    //Click Accept Button
    $scope.acceptCall = function() {
        var url = "../Backend/setCallingMethod.php";
        var postData = {};
        postData.user_id = $scope.from_user_id;
        postData.status = "Accept";
        $scope.isTalking = true;
        console.log("get call status params: " + JSON.stringify(postData));

        $http.post(url, postData).success(function(response) {
            console.log(response);
        });

        ngDialog.close(outCallDialog);
        connectOpentTok(curStatus.outcoming_call[0].session_id, curStatus.outcoming_call[0].token);
    }


    //Click Reject Button
    $scope.rejectCall = function() {
        var url = "../Backend/setCallingMethod.php";
        var postData = {};
        postData.user_id = $scope.from_user_id;
        postData.status = "Reject";

        console.log("get call status params: " + JSON.stringify(postData));

        $http.post(url, postData).success(function(response) {
            console.log(response);
        });
        ngDialog.close(outCallDialog);
    }



    $scope.call = function() {
        if (isTalking == true) {
            return;
        }
        isTalking = true;
        var url = "../Backend/buddy_call.php";

        var postData = {};
        postData.method = "create";
        postData.from = $scope.from_user_id;
        postData.to = $scope.to_user_id;
        postData.call_status = "Call";
        console.log("create call params: " + JSON.stringify(postData));

        $http.post(url, postData).success(function(response) {
            console.log("Call Result: -->" + JSON.stringify(response));
            connectOpentTok(response.session_id, response.token);
        });

        callingDialog = ngDialog.openConfirm({
            template: 'callingDialog',
            scope: $scope
        });
        setTimeout(callTimeOut, timeout);
        console.log("Call Dialog Object:-->" + $scope.callingDialog);
    }

    $scope.endCall = function() {
        isTalking = false;
        $scope.isTalking = false;
        session.disconnect();
        deleteCallStatus($scope.from_user_id, $scope.to_user_id);
    }

    $scope.cancelCallingDialog = function() {
        var url = "../Backend/cancel_call.php";

        var postData = {};
        postData.from = $scope.from_user_id;
        $http.post(url, postData).success(function(response) {
            console.log("Call Result: -->" + JSON.stringify(response));
            connectOpentTok(response.session_id, response.token);
        });
        ngDialog.close(callingDialog);
    }





    $scope.loginUser1 = function() {
        $scope.isLogin = true;
        $scope.from_user_id = "user1";
        $scope.to_user_id = "user2";
        $scope.loginInf = "Login as " + $scope.from_user_id + " aleady.";

        var myVar = setInterval(function () {myTimer()}, interval);
        deleteCallStatus($scope.from_user_id, $scope.from_user_id);
    }

    $scope.loginUser2 = function() {
        $scope.isLogin = true;
        $scope.from_user_id = "user2";
        $scope.to_user_id = "user1";
        $scope.loginInf = "Login as " + $scope.from_user_id + " aleady.";

        var myVar = setInterval(function () {myTimer()}, interval);
        deleteCallStatus($scope.from_user_id, $scope.from_user_id);
    }



    function callTimeOut() {
        //deleteCallStatus($scope.from_user_id, $scope.to_user_id);
        isTalking = false;
    }

    function deleteCallStatus(from, to) {
        var postData = {};
        postData.method = "delete";
        postData.from = from;
        postData.to = to;

        var url = "../Backend/buddy_call.php";
        console.log("delete call params: " + JSON.stringify(postData));

        $http.post(url, postData)
            .success(function(response) {
                console.log("Delete Result:-->" + JSON.stringify(response));
            });
    }
});
