<?php

    include("db_engine/DB.php");
    global $db;
    $call_message = json_decode(file_get_contents("php://input"));

    $result = array();
    if ($db->delete('call_status', array('from' => $call_message->from)))
        $result['result'] = "Success";
    else
        $result['result'] = "Fail";
