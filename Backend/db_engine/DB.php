<?php
    include('DB_driver.php');
    include('DB_active_rec.php');
    include('DB_result.php');
    include('mysql_driver.php');
    include('mysql_result.php');
  
    $db_info['hostname'] = 'localhost';
    $db_info['username'] = 'root';
    $db_info['password'] = '';
    
    // create db manually as we did in Wordpress
    $db_info['database'] = 'test';

    //like a function in PHP;mysqli_connect
    global $db;
    $db = new DB($db_info);
    $db->initialize();

?>
