<?php
	include("db_engine/DB.php");
	global $db;
	
	$user_id = $_REQUEST['user_id'];
	$result = $db->get_where("user", array("user_id" => $user_id))->result_array();
	
	if ($result) {
		$db->where("user_id", $user_id);
		$db->update('user', array('last_time' => date("Y-m-d h:i:sa")));
		$res['result'] = 'success';
	} else {
		$db->insert("user", array("user_id" => $user_id, "last_time" => date("Y-m-d h:i:sa")));
		$res['result'] = 'success';
	}
	
	echo json_encode($res);
?>