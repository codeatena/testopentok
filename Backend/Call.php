<?php

require "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

/**
* 
*/
class Call
{
	var $from;
	var $to;
	var $session_id;
	var $token;
	var $call_status;

	function __construct()
	{
		$from = "";
		$to = "";
		$session_id = "";
		$token = "";
		$call_status = "";
	}

	function createRoom()
	{
		global $db;

		//create opentalk room
		$apiKey = '45328092';
		$apiSecret = 'f9cd1f5a6432f1e0e29c53e20d7eb384c9b60ead';
		
		$opentok = new OpenTok($apiKey, $apiSecret);

		// Create a session that attempts to use peer-to-peer streaming:
		$session = $opentok->createSession();

		// A session that uses the OpenTok Media Router:
		$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

		// A session with a location hint:
		$session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

		// An automatically archived session:
		$sessionOptions = array(
			'archiveMode' => ArchiveMode::ALWAYS,
			'mediaMode' => MediaMode::ROUTED
		);
		$session = $opentok->createSession($sessionOptions);


		// Store this sessionId in the database for later use
		$sessionId = $session->getSessionId();
		
		// Generate a Token from just a sessionId (fetched from a database)
		$token = $opentok->generateToken($sessionId);
		// Generate a Token by calling the method on the Session (returned from createSession)
		$token = $session->generateToken();

		// Set some options in a token
		$token = $session->generateToken(array(
			'role'       => Role::MODERATOR,
			'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
			'data'       => 'name=Johnny'
		));
		
		$result[session_id] = $sessionId;
		$result[token] = $token;

		//save it to DB
		$res = $db->insert(Callstatus_table, array(from => $this->from, to => $this->to, session_id => $sessionId, token => $token, status => $this->call_status ));
		if ($res > 0) {
			$result[status] = success;
		} else {
			$result[status] = fail;
		}
		return $result;
	}

	function getCallStatus()
	{
		global $db;

		$res_from = $db->get_where(Callstatus_table, array(from => $this->from ))->result_array();
		$res_to = $db->get_where(Callstatus_table, array(to => $this->from ))->result_array();

		$result[incoming_call] = $res_from;
		$result[outcoming_call] = $res_to;

		return $result;
	}

	function updateCallStatus()
	{
		global $db;

		$res = $db->update(Callstatus_table, array(status => $this->call_status), array( from => $this->from, to => $this->to ));

		if ($res == false) {
			$result[status] = fail;
		} else {
			$result[status] = success;
		}

		return $result;
	}

	function removeCallStatus()
	{
		global $db;

		if ($this->from == $this->to) {
			$res = $db->delete(Callstatus_table, array(from => $this->from));
			$res = $db->delete(Callstatus_table, array(to => $this->to));
		} else {
			$res = $db->delete(Callstatus_table, array(from => $this->from, to => $this->to ));
		}

		if ($res) {
			$result[status] = success;
		} else {
			$result[status] = fail;
		}
		
		return $result;
	}
}

?>