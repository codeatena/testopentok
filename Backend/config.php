<?php
//user table name
define("User_table", "user");

//user table field
define('last_time', 'last_time');

//buddy list request
define("BuddyList", "BuddyList");
define('UserInfo', 'UserInfo');

//buddy status
define("Online", "Online");
define("Offline", "Offline");

//login timeout
define('LoginTimeout', 5);

//call status table name
define("Callstatus_table", "call_status");

//call status request
define('CallStatus', 'CallStatus');

//call request field
define('create_new_call', 'create');
define('read_call_status', 'read');
define('update_call_status', 'update');
define('delete_call', 'delete');
define('user_id', 'user_id');

//buddy list response type
define('buddy_list', 'buddy_list');

//incoming/outcoming call response type
define('incoming_call', 'incoming_call');
define('outcoming_call', 'outcoming_call');

//call response field
define('success', 'success');
define('fail', 'fail');
define('status', 'status');

// define('from', 'from');
// define('to', 'to');
// define('session_id', 'session_id');
// define('token', 'token');
// define('status', 'status');

//call status table field
define('from', 'from');
define('to', 'to');
define('session_id', 'session_id');
define('token', 'token');

?>