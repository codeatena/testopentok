<?php
	//include config file
	include("config.php");

	include("db_engine/DB.php");
	global $db;

	include("Call.php");

	$buddy = json_decode(file_get_contents("php://input"));

	$user = $db->get_where(User_table, array(user_id => $buddy->user_id ))->result_array();

	//create new user
	if ( count($user) <= 0 ) {
		//create new user
		$res_create = $db->insert(User_table, array(user_id => $buddy->user_id, last_time => date("Y-m-d h:i:sa")));

		if ($res_create < 0) {
			echo json_encode($result[status] = fail);
			return;
		}
	} else {
        $db->where(array(user_id => $buddy->user_id));
        $db->update(User_table, array(last_time => date("Y-m-d h:i:sa")));
    }

	//get user list
	$user_list = $db->get_where(User_table)->result_array();
	$res_online_list = array();
	foreach ($user_list as $_user) {
		if ($_user[user_id] == $buddy->user_id) {
			continue;
		}
		$time_gap = abs(strtotime($_user[last_time]) - strtotime("now"));

		if ($time_gap < LoginTimeout) {
			$online_user[$_user[user_id]] = Online; 
		} else {
			$online_user[$_user[user_id]] = Offline;
		}

		$res_online_list[] = $online_user;
		$online_user = "";
	}

	//get call status
	$buddy_call = new Call();
	$buddy_call->from = $buddy->user_id;
	$res_call_list = $buddy_call->getCallStatus();
	$res[buddy_list] = $res_online_list;
	$res[incoming_call] = $res_call_list[incoming_call];
	$res[outcoming_call] = $res_call_list[outcoming_call];

	echo json_encode($res);
