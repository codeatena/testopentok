<?php
	//include config file
	include("config.php");

	include("db_engine/DB.php");
	global $db;
	
	include("Call.php");

	$buddy_call = new Call();

	$call_message = json_decode(file_get_contents("php://input"));


	$method = $call_message->method;

	if ($call_message->method == create_new_call) {
		$buddy_call->from = $call_message->from;
		$buddy_call->to = $call_message->to;
		$buddy_call->call_status = $call_message->call_status;
	} else if ($call_message->method == read_call_status) {
		$buddy_call->from = $call_message->user_id;
	} else if ($call_message->method == update_call_status) {
		$buddy_call->from = $call_message->from;
		$buddy_call->to = $call_message->to;
		$buddy_call->call_status = $call_message->call_status;
	} else if ($call_message->method == delete_call) {
		$buddy_call->from = $call_message->from;
		$buddy_call->to = $call_message->to;
	}

	$result = "";

	if ( $method == create_new_call ) {
		$result = $buddy_call->createRoom();
	} else if ( $method == read_call_status ) {
		$result = $buddy_call->getCallStatus();
	} else if ( $method == update_call_status ) {
		$result = $buddy_call->updateCallStatus();
	} else if ( $method == delete_call ) {
		$result = $buddy_call->removeCallStatus();
	}

	echo json_encode($result);

?>